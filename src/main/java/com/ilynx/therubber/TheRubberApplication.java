package com.ilynx.therubber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheRubberApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheRubberApplication.class, args);
	}
}
