package com.ilynx.therubber.tire;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author Igor Khlaponin
 */

@Service
@Transactional(readOnly = true)
public class TireServiceImpl implements TireService {

    @Autowired
    private TireRepository tireRepository;

    private Logger logger = Logger.getLogger(TireServiceImpl.class);

    @Override
    @Transactional
    public boolean saveOrUpdate(Tire tire) {
        if (tire == null) {
            logger.debug("Tire is null. Item isn\'t saved");
            return false;
        }

        if (tire.getId() == null) {
            tireRepository.save(tire);
            logger.info(tire.getName() + " has been saved to the DB");
            return true;
        }

        if (tireRepository.findOne(tire.getId()) != null) {
            tireRepository.save(tire);
            logger.info(tire.getName() + " has been updated in the DB");
            return true;
        }

        logger.debug("Tire wasn\'t saved");
        return false;
    }

    @Override
    public Tire get(Long id) {
        return tireRepository.getOne(id);
    }

    @Override
    public List<Tire> getTiresByWidth(Integer width) {
        return tireRepository.getTiresByWidth(width);
    }

    @Override
    public List<Tire> getTiresByProfile(Integer profile) {
        return tireRepository.getTiresByProfile(profile);
    }

    @Override
    public List<Tire> getTiresByRadius(Integer radius) {
        return tireRepository.getTiresByRadius(radius);
    }

    @Override
    public List<Tire> getTiresByParams(Integer width, Integer profile, Integer radius) {
        return tireRepository.getTiresByParams(width, profile, radius);
    }

    @Override
    public List<Tire> getTiresBySeason(Season season) {
        return tireRepository.getTiresBySeason(season);
    }

    @Override
    public List<Tire> getTiresByType(TireType type) {
        return tireRepository.getTiresByType(type);
    }

    @Override
    public List<Tire> getTiresInBasket() {
        throw new UnsupportedOperationException("Unsupported operation"); //TODO fix it
    }

    @Override
    public List<Tire> getAllTires() {
        return tireRepository.findAll();
    }

    @Override
    @Transactional
    public boolean deleteTireById(Long id) {
        if (tireRepository.getOne(id) == null) {
            logger.debug("Tire with id = " + id + "does not exist");
            return false;
        } else {
            logger.info("Item # " + id + "has been deleted");
            tireRepository.delete(id);
            return true;
        }
    }
}
