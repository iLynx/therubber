package com.ilynx.therubber.tire;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Igor Khlaponin
 */

@Entity(name = "Tire")
@Table(name = "tires")
public class Tire implements Serializable {

    private Long id;
    private String brand;
    private String name;
    private Integer width;
    private Integer profile;
    private Integer radius;
    private Season season;
    private TireType type;
    private Double price;
    private String description;
    private String image; //path to the image
    private boolean inBasket;

    public Tire(){}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "brand")
    @NotNull
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Column(name = "tire_name")
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "tire_width")
    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @Column(name = "tire_profile")
    public Integer getProfile() {
        return profile;
    }

    public void setProfile(Integer profile) {
        this.profile = profile;
    }

    @Column(name = "tire_radius")
    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "season")
    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "tire_type")
    public TireType getType() {
        return type;
    }

    public void setType(TireType type) {
        this.type = type;
    }

    @Column(name = "price", columnDefinition="Decimal(10,2) default '0.00'") @Min(0)
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "tire_image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Transient
    public boolean isInBasket() {
        return inBasket;
    }

    public void setInBasket(boolean inBasket) {
        this.inBasket = inBasket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tire)) return false;

        Tire tire = (Tire) o;

        if (!id.equals(tire.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return brand + " " + name;
    }
}
