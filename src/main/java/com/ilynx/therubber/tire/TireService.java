package com.ilynx.therubber.tire;

import java.util.List;

/**
 * @author Igor Khlaponin
 */
public interface TireService {

    boolean saveOrUpdate(Tire tire);

    Tire get(Long id);

    List<Tire> getTiresByWidth(Integer width);

    List<Tire> getTiresByProfile(Integer profile);

    List<Tire> getTiresByRadius(Integer radius);

    List<Tire> getTiresByParams(Integer width, Integer profile, Integer radius);

    List<Tire> getTiresBySeason(Season season);

    List<Tire> getTiresByType(TireType type);

    List<Tire> getTiresInBasket();

    List<Tire> getAllTires();

    boolean deleteTireById(Long id);
}
