package com.ilynx.therubber.tire.controller;

import com.ilynx.therubber.tire.Season;
import com.ilynx.therubber.tire.Tire;
import com.ilynx.therubber.tire.TireService;
import com.ilynx.therubber.tire.TireType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;


/**
 * Rest API for Tires
 *
 * @author Igor Khlaponin
 *         <p>
 *         GET		/tires/{id}                     	get tire by id
 *         GET		/tires/getAll                   	get list of all tires
 *         GET		/tires/inBasket                 	get tires in basket
 *         GET		/tires/getByWidth/{width}			get tires by width
 *         GET		/tires/getByProfile/{profile}		get tires by profile
 *         GET		/tires/getByRadius/{radius}			get tires by radius
 *         GET		/tires/getByParams      			get tires by params
 *         GET		/tires/getBySeason/{season}			get tires by season
 *         GET		/tires/getByType/{type} 			get tires by type
 *         PUT		/tires/new  						add new tire
 *         DELETE	/tires/remove/{id}					delete tire by id
 */

@RestController
@RequestMapping("/tires")
public class TireRestController {

    private Logger logger = Logger.getLogger(TireRestController.class);

    @Autowired
    private TireService tireService;


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Tire> getTireById(@PathVariable("id") Long tireId) {
        Tire tire = tireService.get(tireId);

        if (tire == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tire, HttpStatus.OK);
    }

    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getAllTires() {
        List<Tire> allTires = tireService.getAllTires();

        if (allTires == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(allTires, HttpStatus.OK);
    }

    @GetMapping(value = "/inBasket", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTiresInBasket() {
        //TODO what about session attribute?
        List<Tire> inBasket = tireService.getTiresInBasket();

        if (inBasket == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(inBasket, HttpStatus.OK);
    }

    @GetMapping(value = "/getByWidth/{width}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTireByWidth(@PathVariable("width") int width) {
        List<Tire> tiresByWidth = tireService.getTiresByWidth(width);

        if (tiresByWidth == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tiresByWidth, HttpStatus.OK);
    }

    @GetMapping(value = "/getByProfile/{profile}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTireByProfile(@PathVariable("profile") int profile) {
        List<Tire> tiresByProfile = tireService.getTiresByProfile(profile);

        if (tiresByProfile == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tiresByProfile, HttpStatus.OK);
    }

    @GetMapping(value = "/getByRadius/{radius}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTireByRadius(@PathVariable("radius") int radius) {
        List<Tire> tiresByRadius = tireService.getTiresByRadius(radius);

        if (tiresByRadius== null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tiresByRadius, HttpStatus.OK);
    }

    @GetMapping(value = "/getByParams", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTireByParams(@RequestParam("width") String width,
                                                      @RequestParam("profile") String profile,
                                                      @RequestParam("radius") String radius) {
        List<Tire> tiresByParams = tireService.getTiresByParams(Integer.parseInt(width),
                                                                Integer.parseInt(profile),
                                                                Integer.parseInt(radius));

        if (tiresByParams == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tiresByParams, HttpStatus.OK);
    }

    @GetMapping(value = "/getBySeason/{season}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTireBySeason(@PathVariable("season") String season) {

        List<Tire> tiresBySeason = tireService.getTiresBySeason(Season.get(season));

        if (tiresBySeason == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tiresBySeason, HttpStatus.OK);
    }

    @GetMapping(value = "/getByType/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Tire>> getTireByType(@PathVariable("type") String type) {

        List<Tire> tiresByType = tireService.getTiresByType(TireType.get(type));

        if (tiresByType == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tiresByType, HttpStatus.OK);
    }

    @PutMapping(value = "/new", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTire(@RequestBody  @Valid Tire tire, BindingResult bindingResult,
                                             UriComponentsBuilder builder, HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            logger.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String appPath = request.getRequestURL().substring(0, request.getRequestURL().indexOf("/tires/new"));
        tire.setImage(appPath + "/resources/images/default_tire.jpg");
        tireService.saveOrUpdate(tire);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/{id}").
                buildAndExpand(tire.getId()).toUri());
        logger.info("HttpStatus: CREATED");
        logger.info("Tire with brand " + tire.getBrand() + " and name " + tire.getName() + " has been created");

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/remove/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deletePlayer(@PathVariable("id") Long tireId) {
        if (tireService.get(tireId) != null) {
            tireService.deleteTireById(tireId);
            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            logger.info("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
