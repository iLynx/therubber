package com.ilynx.therubber.tire;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Igor Khlaponin
 */

@Repository
public interface TireRepository extends JpaRepository<Tire, Long> {

    List<Tire> getTiresByWidth(Integer width);

    List<Tire> getTiresByProfile(Integer profile);

    List<Tire> getTiresByRadius(Integer radius);

    @Query("select t from Tire t where t.width = ?1 and t.profile = ?2 and t.radius = ?3")
    List<Tire> getTiresByParams(Integer width, Integer profile, Integer radius);

    List<Tire> getTiresBySeason(Season season);

    List<Tire> getTiresByType(TireType type);

}

