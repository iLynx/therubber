package com.ilynx.therubber.tire;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Igor Khlaponin
 */
public enum Season {
    WINTER("winter"),
    SUMMER("summer"),
    UNIVERSAL("universal");

    private final String literal;

    private static final Map<String, Season> seasons = new HashMap<>();

    static {
        for (Season season : Season.values()) {
            seasons.put(season.getSeasonLiteral(), season);
        }
    }

    private Season(final String literal) {
        this.literal = literal;
    }

    public String getSeasonLiteral() {
        return literal;
    }

    public static Season get(String literal) {
        return seasons.get(literal);
    }
}
