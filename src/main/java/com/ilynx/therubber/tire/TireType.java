package com.ilynx.therubber.tire;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Igor Khlaponin
 */
public enum TireType {
    STUDDED("studded"),
    NON_STUDDED("non-studded"),
    PREPARED_FOR_STUDDING("prepared");

    private final String literal;

    private static final Map<String, TireType> literals = new HashMap<>();

    static {
        for (TireType typeType : TireType.values()) {
            literals.put(typeType.getTypeLiteral(), typeType);
        }
    }

    private TireType(final String literal) {
        this.literal = literal;
    }

    public String getTypeLiteral() {
        return literal;
    }

    public static TireType get(String literal) {
        return literals.get(literal);
    }

}
