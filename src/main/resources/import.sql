INSERT INTO public.tires
(id, brand, tire_name, tire_width, tire_profile, tire_radius, season, tire_type, price, description, tire_image)
VALUES
(1001, 'Nokian', 'Hakka Green', 155, 65, 14, 'SUMMER', 'NON_STUDDED', 694.00, 'no description', 'https://shiny-diski.com.ua/media/images/Hakka_Green_image_1_4_h9n45ZC.500x500.jpeg'),
(1002, 'Nokian', 'Hakka Green', 175, 70, 13, 'SUMMER', 'NON_STUDDED', 767.00, 'no description', 'https://shiny-diski.com.ua/media/images/Hakka_Green_image_1_4_h9n45ZC.500x500.jpeg'),
(1003, 'Nokian', 'Hakka i3', 175, 70, 13, 'SUMMER', 'NON_STUDDED', 777.00, 'no description', 'https://shiny-diski.com.ua/media/images/Hakka_i3_image_1_4.500x500.jpeg'),
(1004, 'Nokian', 'Nordman SX', 165, 65, 14, 'SUMMER', 'NON_STUDDED', 847.00, 'no description', 'https://shiny-diski.com.ua/media/images/Nordman_SX_image_1_4_iEjgPKh.500x500.jpeg'),
(1005, 'Nokian', 'W+', 185, 60, 14, 'WINTER', 'NON_STUDDED', 847.00, 'no description', 'https://shiny-diski.com.ua/media/images/W_image_1_6_xPt7iwV.500x500.jpeg'),
(1006, 'Nokian', 'Nordman RS', 175, 65, 14, 'WINTER', 'NON_STUDDED', 955.00, 'no description', 'https://shiny-diski.com.ua/media/images/Nordman_RS_image_1_3.500x500.jpeg'),
(1007, 'Cordiant', 'Polar SL', 175, 65, 14, 'WINTER', 'NON_STUDDED', 672.00, 'no description', 'https://shiny-diski.com.ua/media/images/Cordiant_Polar_SL.500x500.jpg'),
(1008, 'Cordiant', 'Sno-Max', 175, 65, 14, 'WINTER', 'PREPARED_FOR_STUDDING', 680.00, 'no description', 'https://shiny-diski.com.ua/media/images/Sno-Max_image_1_3_3pist8c.500x500.jpeg'),
(1009, 'Cordiant', 'Polar 2', 185, 60, 14, 'WINTER', 'STUDDED', 820.00, 'no description', 'https://shiny-diski.com.ua/media/images/Polar_2_image_1_3_02Rx1Ah.500x500.jpeg'),
(1010, 'Cordiant', 'Road Runner PS-1', 175, 65, 14, 'SUMMER', 'NON_STUDDED', 750.00, 'no description', 'https://shiny-diski.com.ua/media/images/Road_Runner_PS-1_image_1_3_B2hWAI4.500x500.jpeg'),
(1011, 'Michelin', 'Alpin A4', 175, 65, 14, 'WINTER', 'NON_STUDDED', 1380.00, 'no description', 'https://shiny-diski.com.ua/media/images/Alpin_A4_image_1_4.500x500.jpeg'),
(1012, 'Michelin', 'Alpin A5', 195, 65, 15, 'WINTER', 'NON_STUDDED', 1395.00, 'no description', 'https://shiny-diski.com.ua/media/images/michelin_alpin_5_FyhslE6.500x500.jpg'),
(1013, 'Yokohama', 'Ice Guard IG35', 185, 60, 15, 'WINTER', 'STUDDED', 918.00, 'no description', 'https://shiny-diski.com.ua/media/images/Ice_Guard_IG35_image_1_3_4NZYz8E.500x500.jpeg'),
(1014, 'Yokohama', 'Ice Guard IG35', 175, 65, 14, 'WINTER', 'PREPARED_FOR_STUDDING', 951.00, 'no description', 'https://shiny-diski.com.ua/media/images/Ice_Guard_IG35_image_1_3_4NZYz8E.500x500.jpeg'),
(1015, 'Yokohama', 'W.Drive V905', 185, 60, 15, 'WINTER', 'NON_STUDDED', 1024.00, 'no description', 'https://shiny-diski.com.ua/media/images/yokohama-w-drive-v905_buen3w0.500x500.jpg'),
(1016, 'Rosava', 'BC-48 Capitan', 175, 70, 13, 'UNIVERSAL', 'NON_STUDDED', 524.00, 'no description', 'https://shiny-diski.com.ua/media/images/Rosava-BC-48.500x500.jpg'),
(1017, 'Rosava', 'BC-20', 175, 70, 13, 'UNIVERSAL', 'NON_STUDDED', 539.00, 'no description', 'https://shiny-diski.com.ua/media/images/D091D186-20_image_1_4.500x500.jpeg'),
(1018, 'Rosava', 'SNOWGARD', 175, 65, 14, 'WINTER', 'PREPARED_FOR_STUDDING', 574.00, 'no description', 'https://shiny-diski.com.ua/media/images/Rosava-SNOWGARD_1_lDGJsex.500x500.jpg'),
(1019, 'Rosava', 'TRL-501', 155, 70, 13, 'SUMMER', 'NON_STUDDED', 471.00, 'no description', 'https://shiny-diski.com.ua/media/images/Rosava_TRL-501.500x500.jpg'),
(1020, 'Rosava', 'QuaRtum S49', 175, 70, 13, 'SUMMER', 'NON_STUDDED', 501.00, 'no description', 'https://shiny-diski.com.ua/media/images/QuaRtum_S49_image_1_4_ATO585W.500x500.jpeg');

